import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";
import plugin from 'tailwindcss/plugin';

/** @type {import('tailwindcss').Config} */
export default {
  content: [ "./docs/**/*.md" ],
  blocklist: [
    'text-from-<fontSize>',
    'text-from-[<fontSize>]',
    'text-from-@<screen>',
    'text-from-[@<screen>]',
    'text-from-<fontSize>@<screen>',
    'text-from-[<fontSize>@<screen>]',
    'text-to-<fontSize>',
    'text-to-[<fontSize>]',
    'text-to-[@<screen>]',
    'text-to-@<screen>',
    'text-to-<fontSize>@<screen>',
    'text-to-[<fontSize>@<screen>]',
    'text-relative-unit-<unit>',
  ],
  plugins: [
    fluidFontSizePlugin,
    plugin(function({ addBase }) {
      addBase({
        '.VPNavBarTitle .title': {
          '@apply text-fluid md:text-from-[9px] md:text-to-[base@lg]': {},
        },
      })
    }),
  ],
}
