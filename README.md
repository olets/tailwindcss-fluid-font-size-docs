# tailwindcss-fluid-font-size-docs

Source for [tailwindcss-fluid-font-size](https://codeberg.org/olets/tailwindcss-fluid-font-size)'s documentation. Read the documentation at <https://tailwindcss-fluid-font-size.olets.dev/>.

## Development

1. Install [bun](https://bun.sh/)

1. Install dependencies:

    ```shell
    bun install
    ```

1. Run the site in dev mode:

    ```shell
    bun dev
    ```

The live site deploys from a private repo mirror for 👋 tech reasons 👋. Maintainers should push to both. One convenient way is to add it to `origin` as an additional push url — see <https://hachyderm.io/@olets/111977918256185610>.

## Contributing

> Looking to contribute to tailwindcss-fluid-font-size itself, not the documentation? Go to https://codeberg.org/olets/tailwindcss-fluid-font-size

Thanks for your interest. Contributions are welcome!

> Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

Check the [Issues](https://codeberg.org/olets/tailwindcss-fluid-font-size-docs/issues) to see if your topic has been discussed before or if it is being worked on.

Please read [CONTRIBUTING.md](CONTRIBUTING.md) before opening a pull request.

## License

<a href="https://www.codeberg.org/olets/tailwindcss-fluid-font-size-docs">tailwindcss-fluid-font-size-docs</a> by <a href="https://www.codeberg.org/olets">Henry Bley-Vroman</a> is licensed under a license which is the unmodified text of <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">CC BY-NC-SA 4.0</a> and the unmodified text of a <a href="https://firstdonoharm.dev/build?modules=eco,extr,media,mil,sv,usta">Hippocratic License 3</a>. It is not affiliated with Creative Commons or the Organization for Ethical Source.

Human-readable summary of (and not a substitute for) the [LICENSE](LICENSE) file:

You are free to

- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material

Under the following terms

- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- Non-commercial — You may not use the material for commercial purposes.
- Ethics - You must abide by the ethical standards specified in the Hippocratic License 3 with Ecocide, Extractive Industries, US Tariff Act, Mass Surveillance, Military Activities, and Media modules.
- Preserve terms — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
- No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## Acknowledgments

- Human-readable license summary is modified from https://creativecommons.org/licenses/by-nc-sa/4.0. The ethics point was added.
- Favicon based on [Expand](https://game-icons.net/1x1/delapouite/expand.html) by [Delapouite](https://delapouite.com/), licensed [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), adapted with [Game-icons.net](https://game-icons.net/).
