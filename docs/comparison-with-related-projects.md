# Comparison with related projects

There are other Tailwind plugins in this space. Here's how `tailwindcss-fluid-font-size` compares to some.

Feature | tailwindcss-fluid-font-size | [fluid-tailwind](https://github.com/barvian/fluid-tailwind) v1.0.3 | [tailwindcss-fluid-type](https://github.com/davidhellmann/tailwindcss-fluid-type/) v2.0.5 | [tailwindcss-fluid-typography](https://github.com/prototypdigital/tailwindcss-fluid-typography) v1.0.4 | [tailwind-fluid-typography](https://github.com/craigrileyuk/tailwind-fluid-typography) v1.2.0
---|---|---|---|---|---
Fluid font sizes can go from **px to rem** and from **rem to px** | ✅ Yes [^1] | 🚫 No | 🚫 No | 🚫 No | 🚫 No
Fluid font sizes' **relative dimensions** can be something other than viewport width or container inline size | ✅ Yes | 🚫 No | 🚫 No | 🚫 No | 🚫 No
Fluid font sizes can have **distinct screens** | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Fluid font size utilities can style **font-weight** | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Fluid font size can be **inversely proportional to relative unit** | ✅ Yes | ✅ Yes | 🚫 No | ❓ Unknown | 🚫 No
Fluid font sizes can be relative to **container inline size** | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Can have both **px-to-px and rem-to-rem** fluid font sizes in the same Tailwind config | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Fluid font size values use Tailwind core **font-size value shape** [^2] | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Aware of Tailwind **`fontSize` config** | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Aware of Tailwind **`screen` config** | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No | 🚫 No
Fluid font size utilities can style **letter-spacing** | ✅ Yes | ✅ Yes | 🚫 No | ✅ Yes | 🚫 No
Fluid font sizes can go from **px to px** | ✅ Yes [^1] | ✅ Yes | ✅ Yes | 🚫 No | 🚫 No
Fluid font sizes can go from **rem to rem** | ✅ Yes | ✅ Yes | ✅ Yes | ✅ Yes | 🚫 No | ✅ Yes
Fluid font size utilities can style **line-height** | ✅ Yes | ✅ Yes | ✅ Yes | ✅ Yes | ✅ Yes
Can **redefine** existing font size utilities as fluid | 🚫 No | 🚫 No | ✅ Yes | 🚫 No | 🚫 No
Adds fluidity support to **core plugins besides font-size** | 🚫 No [^3] | ✅ Yes | 🚫 No [^3] | 🚫 No [^3] | 🚫 No [^3]
Can add fluidity support to **third-party plugins** | 🚫 No | ✅ Yes | 🚫 No | 🚫 No | 🚫 No

[^1]: Provided value can be px or rem. In the resulting CSS, px values are transformed to rem values.

[^2]: `fontSize` \| `[fontSize, lineHeight]` \| `[fontSize, { lineHeight?, letterSpacing?, fontWeight? }]`

[^3]: Consider using [`css-fluid-length`](https://codeberg.org/olets/css-fluid-length)
