# Installation and setup

## Installation

The plugin is available from JSR or <Badge type="warning">since 3.0.0</Badge> from NPM. To install it, run one of the below, depending on your runtime and package manager:

### NPM

::: code-group

```sh [bun]
bun add -D @olets/tailwindcss-fluid-font-size
```

```sh [deno]
deno add npm:@olets/tailwindcss-fluid-font-size
```

```sh [npm]
npm add -D @olets/tailwindcss-fluid-font-size
```

```sh [pnpm]
pnpm add -D @olets/tailwindcss-fluid-font-size
```

```sh [yarn]
yarn add -D @olets/tailwindcss-fluid-font-size
```

:::

### JSR

::: danger
Deprecated, may be removed from JSR in a future major version.
:::

::: code-group

```sh [bun]
bunx jsr add -D @olets/tailwindcss-fluid-font-size
```

```sh [deno]
deno add @olets/tailwindcss-fluid-font-size
```

```sh [npm]
npx jsr add -D @olets/tailwindcss-fluid-font-size
```

```sh [pnpm]
pnpm dlx jsr add -D @olets/tailwindcss-fluid-font-size
```

```sh [yarn]
yarn dlx jsr add -D @olets/tailwindcss-fluid-font-size
```

:::

## Setup

Then add the plugin to your Tailwind configuration file.

:::tip
This example uses `import` and JS, but you can also use `require` and/or TS. See the official [Tailwind configuration docs](https://tailwindcss.com/docs/configuration).
:::

::: warning Heads Up
In v1.x and v2.x, you could

```js
import { fluidFontSizePlugin } from "@olets/tailwindcss-fluid-font-size";
```

In 3.x, you can't.
:::

::: code-group

```js [tailwind.config.js]
import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";

/** @type {import('tailwindcss').Config} */
module.exports = {
  theme: {
    // ...
  },
  plugins: [
    fluidFontSizePlugin,
    // ...
  ],
}
```

:::
