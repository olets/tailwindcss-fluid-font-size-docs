# API

## Configuration

> See [Customizing: Font sizes and screens](/customizing/configuring-font-sizes-and-screens) and [Customizing: Options](/customizing/options).

::: code-group

```js [Tailwind config]
theme: {
  // Tailwind core plugin keys
  fontSize: {
    // fontSizes available to text-from-* and text-to-* utility classes
    key: /* fontSize */,
  },
  screens: {
    // screens available to text-from-* and text-to-* utility classes
    key: /* screen */,
  },
  
  // tailwindcss-fluid-font-size keys
  fontSizeFrom: {
    // fontSizes available to text-from-* utility classes
    key: /* fontSize | { fontSize?, screen? } */,
  },
  fontSizeFromScreens: {
    // screens available to text-from-* utility classes
    key: /* screen */,
  },
  fontSizeScreens: {
    // screens available to text-from-* and text-to-* utility classes
    key: /* screen */,
  },
  fontSizeTo: {
    // fontSizes available to text-to-* utility classes
    key: /* fontSize | { fontSize?, screen? } */,
  },
  fontSizeToScreens:{
    // screens available to text-to-* utility classes
    key: /* screen */,
  }
},
plugins: [
  fluidFontSizePlugin({
    fromScreen?,
    fromFontSize?,
    pretty?,
    relativeUnit?,
    toScreen?,
    toFontSize?,
  }),
],
```
:::

## DOM

### Font sizes and screens

See [Usage: Font sizes and screens](/usage/font-sizes-and-screens)

```html
<div 
  class="text-fluid"
>…</div>
```

```html
<div 
  class="text-fluid text-from-<fontSize> text-to-…"
>…</div>
```

```html
<div 
  class="text-fluid text-from-@<screen> text-to-…"
>…</div>
```

```html
<div 
  class="text-fluid text-from-<fontSize>@<screen> text-to-…"
>…</div>
```

```html
<div 
  class="text-fluid text-from-…"
>…</div>
```

```html
<div 
  class="text-fluid text-to-…"
>…</div>
```

### Relative unit

See [Usage: Relative unit](/usage/relative-unit)

```html
<div 
  class="text-fluid text-from-… text-to-… text-relative-unit-<unit>"
>…</div>
```

### Arbitrary values

See [Usage: Arbitrary values](/usage/arbitrary-values)

```html
<div 
  class="text-fluid text-from-[<fontSize>] text-to-…"
>…</div>
```

```html
<div 
  class="text-fluid text-from-[@<screen>] text-to-…"
>…</div>
```

```html
<div 
  class="text-fluid text-from-[<fontSize>@<screen>] text-to-…"
>…</div>
```

### Responsive design

Standard Tailwind

```html
<div 
  class="text-fluid text-from-… text-to-… <screen>:text-from-… <screen>:text-to-…"
>…</div>
```
