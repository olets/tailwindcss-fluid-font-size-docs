# Contributing

Thanks for your interest. Contributions are welcome!

> Please note that both tailwindcss-fluid-font-size and its documentation is released with a Contributor Code of Conduct. By participating in this project you agree to abide by its terms.

To learn more about contributing to tailwindcss-fluid-font-size, see [olets/tailwindcss-fluid-font-size's CONTRIBUTING.md](https://codeberg.org/olets/tailwindcss-fluid-font-size/src/branch/main/CONTRIBUTING.md).

To learn more about contributing to tailwindcss-fluid-font-size's documentation, see [olets/tailwindcss-fluid-font-size-docs's CONTRIBUTING.md](https://codeberg.org/olets/tailwindcss-fluid-font-size-docs/src/branch/main/CONTRIBUTING.md).
