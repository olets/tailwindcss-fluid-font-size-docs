# Customizing Options

You can customize the default "from" or "to" `<fontSize>`s and `<screen>`s (see [Usage: Font sizes and screens](/usage/font-sizes-and-screens)) and the default relative unit (see [Usage: Relative Unit](/usage/relative-unit)).

You can also change how legible the generated CSS is, with the `pretty` option. Setting `pretty` to `false` makes the `.text-fluid` CSS a _little_ lighter (less than 150 bytes regardless of how many font sizes and screens you've configured) but harder to debug. `true` (the default) is recommended for all but the most committed optimizers.

Pass any options to the plugin via its single object prop. All are optional.

::: code-group

```js [tailwind config]
  // …
  plugins: [
    fluidFontSize({
      fromFontSize: '4rem',
      fromScreen: '470px',
      relativeUnit: 'cqi',
      toFontSize: '7rem',
      toScreen: '740px',
    }),
  ]
  // …
```

:::

Option | Type | Default | Notes
---|---|---|---
`fromScreen` | string\|object | Tailwind's `screens.sm` if defined and px, or rem, or a px or rem 'min', or a px or rem 'max'. <br><br> Otherwise, Tailwind's default theme's `screens.sm` | px (e.g. `'47px'`), rem (e.g. `'4.7rem'`), or a min or max Tailwind screen with a px or rem value string `{ min: '470px' }`, `{ min: '47rem' }`, `{ max: '740px' }`, or `{ max: '74rem' }`
`fromFontSize` | string\|array |  Tailwind's `fontSize.base` if defined and px or rem. <br><br> Otherwise, Tailwind's default theme's `fontSize.base` | Tailwind font size-like value with px or rem font size px (e.g. `'47px'`, `'4.7rem'`, `['47px', …]`, `['4.7rem', …]`)
`pretty` | boolean | `true`
`relativeUnit` | string | `'vw'` | The unit the font-size is relative to. `cqb`, `cqh`, `cqi`, `cqmax`, `cqmin`, `cqw`, `dvh`, `dvw`, `vb`, `vh`, `vi`, `vmax`, `vmin`, or `vw`
`toScreen` | string\|object | Tailwind's `screens['2xl']` if defined and px, or rem, or a px or rem 'min', or a px or rem 'max'. <br><br> Otherwise, Tailwind's default theme's `screens['2xl']` | px (e.g. `'47px'`), rem (e.g. `'4.7rem'`), or a min or max Tailwind screen with a px or rem value string `{ min: '470px' }`, `{ min: '47rem' }`, `{ max: '740px' }`, or `{ max: '74rem' }`
`toFontSize` | string\|array |  Tailwind's `fontSize.xl` if defined and px or rem. <br><br> Otherwise, Tailwind's default theme's `fontSize.xl` | Tailwind font size-like value with px or rem font size px (e.g. `'47px'`, `'4.7rem'`, `['47px', …]`, `['4.7rem', …]`)
