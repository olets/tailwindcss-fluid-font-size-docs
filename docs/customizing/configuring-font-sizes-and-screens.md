# Configuring font sizes and screens

- **You can configure "to" and "from" `<fontSize>`s** using the `theme.fontSize` section of your Tailwind config file.

  You can configure your own custom set of "from" `<fontSize>`s using the `theme.fontSizeFrom` section of your Tailwind config file, and your own custom set of "to" `<fontSize>`s using the `theme.fontSizeTo` section of your Tailwind config file.

  `theme.fontSizeFrom` and `theme.fontSizeTo` values accept a `<screen>`. See [Usage: Font sizes and screens](/usage/font-sizes-and-screens) for screen value requirements and limitations.


- **You can configure "to" and "from" `<screen>`s** using the `theme.screens` section of your Tailwind config file.

  You can also configure them in the `theme.fontSizeScreens` section of your Tailwind config file. Use valid `theme.screens` values.

  And you can configure "from" `<screen>`s using the `theme.fontSizeFromScreens` section of your Tailwind config file, and "to" `<screen>`s using the `theme.fontSizeToScreens` section. Use valid `theme.screens` values.

::: warning
See [Usage: Font sizes and screens](/usage/font-sizes-and-screens) for value requirements and limitations.
:::

::: code-group

```js [tailwind config]
  // …
  theme: {
    extends: {
      fontSize: {
        // this is vanilla Tailwind
        // can use as <fontSize> in text-from-* and text-to-*
        
        a: '47px',
        b: ['7rem', '4'],
        c: ['40px', { fontWeight: '7' }],
      },
      screens: {
        // this is vanilla Tailwind
        // can use as <screen> in text-from-* and text-to-*

        d: '470px',
        e: { min: '740px' },
        f: { max: '74rem' },
      },
    },
    fontSizeFrom: ({ theme }) => ({
      // additional text-from-* <fontSize>s

      g: ['4rem', '7'],
      h: {
        fontSize: theme('fontSize.3xl'),
        screen: '714px',
      }
    }),
    fontSizeFromScreens: {
      // additional text-from-* <screen>s

      i: '77rem',
    },
    fontSizeScreens: {
      // additional text-from-* and text-to-* <screen>s
      // in case of key conflict with fontSizeFromScreens or fontSizeToScreens,
      // fontSize(From|To)Screens wins out over fontSizeScreens

      j: '474px',
    },
    fontSizeTo: ({ theme }) => ({
      // additional text-to-* <fontSize>s

      k: {
        fontSize: ['47px', {
          lineHeight: '40px',
          fontWeight: '500',
        }],
        screen: theme('screens.xl'),
      }
    }),
    fontSizeToScreens: {
      // additional text-to-* <screen>s

      l: { min: '747px' },
    },
  },
  // …
```

:::
