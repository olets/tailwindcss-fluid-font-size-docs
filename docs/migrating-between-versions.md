# Migrating between versions

## v2.x to v3.0.0

The package now has only a default export, not both a default export and a named export.

```js
import { fluidFontSizePlugin } from "@olets/tailwindcss-fluid-font-size";  // [!code --]
import fluidFontSizePlugin from "@olets/tailwindcss-fluid-font-size";  // [!code ++]
```

Read <https://rollupjs.org/configuration-options/#output-exports> for motivation.
