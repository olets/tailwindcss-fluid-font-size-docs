---
titleTemplate: :title # see also VitePress config
---

# tailwindcss-fluid-font-size

With `tailwindcss-fluid-font-size`, fluid font size in Tailwind CSS (v3.3.2 and above) is straightforward to configure, write, read, and maintain.

What's fluid fluid size? Font size that's continuously responsive to a relative dimension (window width for example), as opposed to traditional, stepwise responsive CSS, where the styles change at distinct breakpoints.

<div class="px-4 py-1">
<div class="border rounded p-4 text-fluid text-from-base@sm text-to-4xl@lg lg:text-from-4xl@lg lg:text-to-base@xl">

  This sentence is a demo! Try resizing your screen. It's `.text-base` on screens narrower than `sm`, `.text-xl` on screens wider than `xl`, and fluid in between going through `.text-4xl` at screen `lg`.

</div>
</div>

Add the plugin, and with no additional configuration you'll have utility classes for fluid typography going from any of Tailwind's font sizes between any of Tailwind's breakpoints, relative to window width or any other dimension. Customize it to add additional sizes and breakpoints. Intellisense works, responsive variants work, arbitrary classes work.

::: tip
Want fluid lengths for things other than font-size, including for non-Tailwind CSS-in-JS? Check out my utility [css-fluid-length](https://codeberg.org/olets/css-fluid-length)
:::

