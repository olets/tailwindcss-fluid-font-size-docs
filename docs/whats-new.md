# What's new?

Highlights in 3.x

- <Badge type="warning">Since 3.0.0</Badge> [Installation](./fundamentals/installation-and-setup.md#installation): Available on NPM.

- <Badge type="warning">Since 3.0.0</Badge> [Setup](./fundamentals/installation-and-setup.md#setup): No named export.
