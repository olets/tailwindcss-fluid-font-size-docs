# Arbitrary values

## Font sizes and screens

You can use arbitrary values in `text-from-*` and `text-to-*` classes. `[<fontSize>]`, `[@<screen>]`, or `[<fontSize>@<screen>]` are all supported.

`<fontSize>` can be a pixel length (e.g. `16px`), a rem length (e.g. `1rem`), or a key in the Tailwind theme's `fontSize`, `fontSizeFrom`, or `fontSizeTo`.

`<screen>` can be a pixel length (e.g. `16px`), a rem length (e.g. `1rem`), or a key in the Tailwind theme's `screen`, `fontSizeScreens`, `fontSizeFromScreens`, or `fontSizeToScreens`.

The following are some of the possibilities for duplicating the `<div class="text-fluid">` default (`.text-base` on screens narrower than `sm`, `.text-xl` on screens wider than `2xl`, and fluid in between) with arbitrary values.

Resize your window to see the demos' font sizes change.

<div class="space-y-14 mt-8">
<div>

```html
<div class="text-fluid text-from-[16px] text-to-[1.25rem]"></div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-[16px] text-to-[1.25rem]">

Sphinx of black quartz, judge my vow.

</div>
</div>
<div>

```html
<div class="text-fluid text-from-@sm text-to-@2xl"></div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-@sm text-to-@2xl">

The jay, pig, fox, zebra and my wolves quack.

</div>
</div>
<div>

```html
<div class="text-fluid text-from-[@sm] text-to-[@2xl]"></div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-[@sm] text-to-[@2xl]">

Five quacking zephyrs jolt my wax bed.

</div>
</div>
<div>

```html
<div class="text-fluid text-from-[1rem@sm] text-to-[20px@2xl]"></div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-[1rem@sm] text-to-[20px@2xl]">

How quickly daft jumping zebras vex!

</div>
</div>
<div>

```html
<div class="text-fluid text-from-[base@40rem] text-to-[xl@1536px]"></div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-[base@40rem] text-to-[xl@1536px]">

The five boxing wizards jump quickly.

</div>
</div>
</div>

## Relative unit

You can use arbitrary values in `text-relative-unit-*` classes.

```html
<div class="text-fluid text-relative-unit-[somecoolnewunit] text-to-2xl"></div>
```

<!-- usage/DUPE arbitrary-values.md, usage/relative-unit.md -->

::: tip Don't see the unit you need?
🙏 If `tailwindcss-fluid-font-size` is missing a CSS unit font size could be relative to ([MDN list](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Values_and_Units#distance_units)) please open an issue and, if you're up for it, a pull request! See [Contributing](/contributing).
:::
