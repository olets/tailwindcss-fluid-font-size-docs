# Font sizes and screens

Use the CSS class `text-fluid`, optionally in combination with a `text-from-*` class and/or a `text-to-*` class.

<div class="[&>ul]:space-y-20 mt-12">
  <section>

  ## Make font size fluid
  
  Add `.text-fluid` to enable fluid font-size. On its own, it makes font-size

  - the default "from" font-size (`.text-base`) when the window is narrower than the default "from" screen (screen `sm`)

  - fluidly increase font size as window width increases, from default "from" font size (`.text-base`) at default "from" screen (screen `sm`) to default "to" font size (`.text-xl`) at default "to" screen (screen `2xl`)

  - the default "to" font-size (`.text-xl`) when the window is wider than the default "to" screen (screen `2xl`)

  The default font sizes and screens can be configured — see [Customizing: Options](/customizing/options).

  ```html
  <div class="text-fluid"></div>
  ```

  <div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid">

  This sentence is `.text-base` on screens narrower than `sm`, `.text-xl` on screens wider than `2xl`, and fluid in between.

  </div>

  ::: tip Fallbacks
  If your theme doesn't have a `sm` screen, the default Tailwind theme's `sm` screen will be used; same for the `2xl` screen, and the `base` and `xl` font font sizes.
  :::

  </section>
  <section>

  ## Specify font sizes
  
  `text-to-<fontSize>` and `text-from-<fontSize>` classes change the font size(s). `<fontSize>` is a Tailwind font size key.

  Out of the box, you can use all those of your `theme.fontSize`s which have a pixel or rem `font-size` (for example `'10px'`, `['4rem', …]`). (Pixel values will be converted to rems.)
  
  You can also configure additional values — see [Customizing: Font sizes and screens](/customizing/configuring-font-sizes-and-screens).

  ::: danger Accessibility Risk
  Not all fluid font sizes are accessible. Maxwell Barvian's article [Addressing Accessibility Concerns With Using Fluid Type](https://www.smashingmagazine.com/2023/11/addressing-accessibility-concerns-fluid-type/) goes into details.
  :::

  In the following example, the "from" font size is `sm` and the "to" font size is `2xl`:

  ```html
  <div class="text-fluid text-from-sm text-to-2xl">…</div>
  ```

  <div class="border p-4 rounded [&_p]:m-0 overflow-x-auto text-fluid text-from-sm text-to-2xl">
  
  This sentence is `.text-sm` on screens narrower than `sm`, `.text-2xl` on screens wider than `2xl`, and fluid in between.

  </div>

  </section>
  <section>

  ## Specify screens
  
  `text-to-@<screen>` and `text-from-@<screen>` classes change the screen(s). `<screen>` is a Tailwind screen key.

  Out of the box, you can use all those of your `theme.screens`s which are a pixel or rem breakpoint (e.g. `'470px'` or `'47rem'`), or a min-width or max-width breakpoint (e.g. `{ min: '470px' }`, `{ min: '47rem' }`, `{ max: '740px' }`, or `{ max: '74rem' }`). (Pixel values will be converted to rems.) You can also configure additional values — see [Customizing: Font sizes and screens](/customizing/configuring-font-sizes-and-screens).

  In the following example, the "from" font size is the default (font size `base`), the "from" screen is `md`, the "to" font size is the default (font size `2xl`) and the "to" screen is `xl`:

  ```html
  <div class="text-fluid text-from-@md text-to-@xl">…</div>
  ```

  <div class="border p-4 rounded [&_p]:m-0 overflow-x-auto text-fluid text-from-@md text-to-@xl">

  This sentence is `.text-base` on screens narrower than `md`, `.text-2xl` on screens wider than `xl`, and fluid in between.

  </div>

  </section>
  <section>

  ## Specify font sizes and screens
  
  `text-to-<fontSize>@<screen>` and `text-from-<fontSize>@<screen>` classes change the font size(s) and screen(s).

  See above for limitations and requirements of `<fontSize>`s and `<screen>`s.

  In the following example, the "from" font size is `sm`, the "from" screen is screen `md`, the "to" font size is `lg`, and the "to" screen is screen `xl`:
  

  ```html
  <div class="text-fluid text-from-sm@md text-to-lg@xl">…</div>
  ```

  <div class="border p-4 rounded [&_p]:m-0 overflow-x-auto text-fluid text-from-sm@md text-to-lg@xl">

  This sentence is `.text-base` on screens narrower than `sm`, `.text-lg` on screens wider than `xl`, and fluid in between.

  </div>

  </section>
  <section>

  ## Change relationship
  
  The "from" `<fontSize>` can be bigger than the "to" `<fontSize>`. You can make font size decrease as the relative unit increases.

  ```html
  <div class="text-fluid text-from-2xl text-to-base">…</div>
  ```

  <div class="border p-4 rounded [&_p]:m-0 overflow-x-auto text-fluid text-from-2xl text-to-base">

  This sentence is `.text-2xl` on screens narrower than `sm`, `.text-base` on screens wider than `2xl`, and fluid in between.

  </div>

  </section>
  <section>

  ## Inheritance
  
  Nesting works. Use `.text-fluid` to re-enable an ancestor's fluid font size.

  ```html
  <div class="text-fluid text-from-xl text-to-4xl">
    Fluid

    <div class="text-base">
      Not fluid

      <div class="text-fluid">Fluid</div>
    </div>
  </div>
  ```

  <div class="border p-4 rounded [&_p]:m-0 overflow-x-auto text-fluid text-from-xl text-to-4xl">

  Fluid

  <div class="text-base">

  Not fluid

  <div class="text-fluid">
  
  Fluid
  
  </div>
  </div>
  </div>

  </section>
  <section>

  ## Responsive design
  
  Specify stop positions with standard Tailwind responsive design.

  ```html
  <div class="text-fluid text-from-base@md text-to-3xl@lg lg:text-from-3xl@lg lg:text-to-base@xl">…</div>
  ```

  <div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-base@md text-to-3xl@lg lg:text-from-3xl@lg lg:text-to-base@xl">
  
  This sentence is `.text-base` on screens narrower than `md`, `.text-base` on screens wider than `xl`, and fluid in between passing through `.text-3xl` at screen `lg`.

  </div>
  </section>
</div>