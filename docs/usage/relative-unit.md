# Relative unit

By default, the fluid font-sizes are relative to viewport width. Use a `text-relative-unit-*` class to change the relative unit. For example, you could have text grow or shrink relative to `dvh` (responding to the appearance and disappearance of mobile browser UI), or to a container size. Choose between

- viewport units: `vw`, `vh`, `vi`, `vb`, `vmin`, `vmax`
- device units: `dvh`, `dvw`
- container units: `cqw`, `cqh`, `cqi`, `cqb`, `cqmin` `cqmax`

Here's an example relative to container width (`cqw`). (Its custom `<screen>`s aren't essential to the functionality — they're chosen to play nicely with this site's layout.)

```html
<div class="text-fluid text-from-[sm@450px] text-to-[lg@688px] text-relative-unit-cqw">…</div>
```

<div class="[container-type:inline-size]">
<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-[sm@450px] text-to-[lg@688px] text-relative-unit-cqw">

  This sentence is `.text-sm` if its container is narrower than `450px`, `.text-lg` if its container is wider than `688px`, and fluid in between.

</div>
</div>

And here's an example relative to viewport height (`vh`):

```html
<div class="text-fluid text-from-sm text-to-lg text-relative-unit-vh">…</div>
```

<div class="border rounded [&_p]:m-0 overflow-x-auto p-4 text-fluid text-from-sm text-to-lg text-relative-unit-vh">

  This sentence is `.text-sm` if the viewport is shorter than `sm`, `.text-lg` if the viewport is taller than `2xl`, and fluid in between.

</div>

If you use a container unit, don't forget to define a container parent, with `container-type` CSS. `tailwindcss-fluid-font-size` plays nice with the first-party [`tailwindcss-container-queries`](https://github.com/tailwindlabs/tailwindcss-container-queries) plugin, but note that the container plugin (at v0.1.1 as of this writing) always uses `container-type: inline-size`. If you want to make font-size relative to a container's block size, you may need to roll your own plugin or arbitrary class.

<!-- usage/DUPE arbitrary-values.md, usage/relative-unit.md -->

::: tip Don't see the unit you need?
🙏 If `tailwindcss-fluid-font-size` is missing a CSS unit font size could be relative to ([MDN list](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Values_and_Units#distance_units)) please open an issue and, if you're up for it, a pull request! See [Contributing](/contributing).
:::
