import { defineConfig } from "vitepress";
import markdownItFootnote from "markdown-it-footnote";

const title = "tailwindcss-fluid-font-size";
const description =
  "Fluid font sizes utilities plugin for Tailwind 3.";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: title,
  description: description,
  // https://vitepress.dev/reference/site-config#titletemplate
  // DUPE titleTemplate, 2 x transformPageData metaTitle
  titleTemplate: `:title :: ${title}`, // to change delimiter from default pipe to play nice with Fathom event id format. see also homepage frontmatter
  // https://vitepress.dev/reference/default-theme-last-updated
  lastUpdated: true,
  transformPageData(pageData, ctx) {
    function getSocialContent(
      pageData,
      ctx
    ): {
      description: string;
      title: string;
    } {
      const ret = {
        description: pageData.description || ctx.siteConfig.site.description,
        title: "",
      };

      if (pageData.frontmatter.title) {
        ret["title"] = pageData.frontmatter.title;
      } else if (pageData.titleTemplate === ":title") {
        ret["title"] = ctx.siteConfig.site.title;
      } else {
        // DUPE titleTemplate, getSocialContent
        ret["title"] = `${pageData.title} :: ${ctx.siteConfig.site.title}`;
      }

      return ret;
    }

    const socialContent = getSocialContent(pageData, ctx);

    pageData.frontmatter.head ??= [];
    pageData.frontmatter.head.push(
      [
        "meta",
        {
          name: "og:title",
          content: socialContent.title,
        },
      ],
      [
        "meta",
        {
          name: "og:description",
          content: socialContent.description,
        },
      ],
      [
        "meta",
        {
          name: "twitter:title",
          content: socialContent.title,
        },
      ],
      [
        "meta",
        {
          name: "twitter:description",
          content: socialContent.description,
        },
      ]
    );
  },

  head: [
    [
      "script",
      {
        src: "https://cdn.usefathom.com/script.js",
        "data-site": "DDLODNVN",
        defer: "",
      },
    ],
    [
      "link",
      {
        rel: "icon",
        href: "/favicon.ico",
        sizes: "32x32",
      },
    ],
    [
      "link",
      {
        rel: "icon",
        type: "image/svg+xml",
        href: "/icon.svg",
      },
    ],
    [
      "link",
      {
        // 180x180
        rel: "apple-touch-icon",
        href: "/apple-touch-icon.png",
      },
    ],
    [
      "link",
      {
        rel: "manifest",
        href: "/site.webmanifest",
      },
    ],

    // og:title and og:description are set dynamically in transformPageData
    [
      "meta",
      {
        property: "og:image:width",
        content: "1200",
      },
    ],
    [
      "meta",
      {
        property: "og:image:height",
        content: "630",
      },
    ],
    [
      "meta",
      {
        property: "og:image",
        content: "https://tailwindcss-fluid-font-size.olets.dev/ogimage.jpg",
      },
    ],

    // twitter:title and twitter:description are set dynamically in transformPageData
    [
      "meta",
      {
        property: "twitter:card",
        content: "summary_large_image",
      },
    ],
    [
      "meta",
      {
        property: "twitter:image",
        content: "https://tailwindcss-fluid-font-size.olets.dev/ogimage.jpg",
      },
    ],
  ],

  markdown: {
    externalLinks: {
      class: "vp-external-link-icon",
      target: "_self",
    },
    config: (md) => {
      md.use(markdownItFootnote);
    },
    theme: {
      light: "github-light-high-contrast",
      dark: "github-dark-high-contrast",
    },
  },

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config

    // https://vitepress.dev/reference/default-theme-edit-link
    editLink: {
      pattern:
        "https://codeberg.org/olets/tailwindcss-fluid-font-size-docs/src/branch/main/docs/:path",
    },
    logo: {
      alt: "tailwindcss-fluid-font-size Logo",
      src: "/icon.svg",
    },
    nav: [
      {
        text: "Source, Changelog, License",
        link: "https://codeberg.org/olets/tailwindcss-fluid-font-size",
        target: "_self",
      },
      { text: "olets.dev", link: "https://olets.dev", target: "_self" },
    ],

    search: {
      provider: "local",
    },

    sidebar: [
      {
        text: "Fundamentals",
        items: [
          { text: "Overview", link: "/" },
          { text: "What’s New", link: "whats-new" },
          {
            text: "Installation and setup",
            link: "/fundamentals/installation-and-setup",
          },
          { text: "API", link: "/fundamentals/api" },
          {
            text: "Comparison with related projects",
            link: "/comparison-with-related-projects",
          },
        ],
      },
      {
        text: "Usage",
        items: [
          {
            text: "Font sizes and screens",
            link: "/usage/font-sizes-and-screens",
          },
          { text: "Relative unit", link: "/usage/relative-unit" },
          { text: "Arbitrary values", link: "/usage/arbitrary-values" },
        ],
      },
      {
        text: "Customizing",
        items: [
          {
            text: "Configuring font sizes and screens",
            link: "/customizing/configuring-font-sizes-and-screens",
          },
          { text: "Options", link: "/customizing/options" },
        ],
      },
      {
        text: "Migrating between versions",
        link: "/migrating-between-versions",
      },
      {
        text: "Contributing",
        link: "/contributing",
      },
    ],

    socialLinks: [],
  },
});
